from __future__ import print_function

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import GenericAPIView


import pdb

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from iapws import IAPWS97
# Create your views here.
from requests import request
from rest_framework import status, viewsets
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from boilercal.calculations import BoilerEfficiency, BoilerEfficiencyIndirectMethod
from boilercal.serializers import BoilerIndirectVMSerializer, EnthalpySuperHeatedSteamSerializer, \
    EnthalpyForSaturatedSteamSerializer, EnthalpyForSaturatedLiquidSerializer
from userManagement.decorators import verify_token


class GetUsers(APIView):
    @csrf_exempt
    def get(self, request):
        return Response({'user1': 'Aditya', 'user2': 'srikanth'})


# given pressure(P) and temperature (T)
class GetEnthalpyForSuperHeatedSteam(APIView):

    @swagger_auto_schema(request_body=EnthalpySuperHeatedSteamSerializer)
    def post(self, request):
        try:
            check_token = verify_token(request)
            if check_token:
                serializer = EnthalpySuperHeatedSteamSerializer(data=request.data)
                if serializer.is_valid():
                    pressure = serializer.validated_data.get("pressure")
                    temp = serializer.validated_data.get("temperature")
                    enthalpy = IAPWS97(P=pressure, T=temp)
                    return Response({"enthaply": enthalpy.h}, status=200)
                else:
                    return Response({"errors": dict(serializer.errors)}, status=status.HTTP_206_PARTIAL_CONTENT)
            else:
                return Response(False, status=status.HTTP_403_FORBIDDEN)
        except Exception as e:
            return Response({"error": "something went wrong", "message": str(e)}, status=500)


# given pressure(P) and dryness fraction(X) where X=1
class GetEnthalpyForSaturatedSteam(APIView):
    @swagger_auto_schema(request_body=EnthalpyForSaturatedSteamSerializer)
    def post(self, request):
        try:
            check_token = verify_token(request)
            if check_token:
                serializer = EnthalpyForSaturatedSteamSerializer(data=request.data)
                if serializer.is_valid():
                    pressure = serializer.validated_data.get("pressure")
                    enthalpy = IAPWS97(P=pressure, x=1)
                    return Response({"enthaply": enthalpy.h}, status=200)
                else:
                    return Response({"errors": dict(serializer.errors)}, status=status.HTTP_206_PARTIAL_CONTENT)
            else:
                return Response(False, status=status.HTTP_403_FORBIDDEN)
        except Exception as e:
            return Response({"error": "something went wrong", "message": str(e)}, status=500)


# given temperature(T) and dryness fraction(X) where X=0
class GetEnthalpyForSaturatedLiquid(APIView):
    @swagger_auto_schema(request_body=EnthalpyForSaturatedLiquidSerializer)
    def post(self, request):
        try:
            check_token = verify_token(request)
            if check_token:
                serializer = EnthalpyForSaturatedLiquidSerializer(data=request.data)
                if serializer.is_valid():
                    temperature = serializer.validated_data.get("temperature")
                    enthalpy = IAPWS97(T=temperature, x=0)
                    return Response({"enthaply": enthalpy.h}, status=200)
                else:
                    return Response({"errors": dict(serializer.errors)}, status=status.HTTP_206_PARTIAL_CONTENT)
            else:
                return Response(False, status=status.HTTP_403_FORBIDDEN)
        except Exception as e:
            return Response({"error": "something went wrong", "message": str(e.message)}, status=500)


class CalculateBoilerEffDirectMethod(APIView):
    def post(self, request):
        try:
            print (request.data)
            efficiency_cal_inputs = request.data
            boiler_eff_class_inst = BoilerEfficiency(efficiency_cal_inputs.get("SteamFlowPerHour"), efficiency_cal_inputs.get("MainStreamEnthalpy"),
                             efficiency_cal_inputs.get("FeedWaterEnthalpy"), efficiency_cal_inputs.get("MassOfFuelPerHour"),
                             efficiency_cal_inputs.get("GcvOfFuel"))
            efficiency = boiler_eff_class_inst.boiler_efficiency_using_direct_method()
            return Response({"efficiency": efficiency}, status=200)
        except Exception as e:

            return Response({"error": "something went wrong", "message": e.message}, status=500)


class CalculateBoilerEffIndirectMethod(APIView):

    @swagger_auto_schema(request_body=BoilerIndirectVMSerializer)
    def post(self, request):
        try:
            serializer = BoilerIndirectVMSerializer(data=request.data)
            if serializer.is_valid():
                boiler_in_direct_inst = BoilerEfficiencyIndirectMethod(request.data)
                boiler_eff = boiler_in_direct_inst.boiler_eff_indrect_method_nb()
                return Response(dict(Input=dict(serializer.validated_data), Output={
                    "boilerEffeciency": boiler_eff,
                    "tas": boiler_in_direct_inst.calculate_tas(),
                    "ea": boiler_in_direct_inst.calculate_excess_air_supplied_ea(),
                    "aas": boiler_in_direct_inst.cal_actual_mass_of_air_supplied_aas(),
                    "totalmassOfFuel": boiler_in_direct_inst.total_mass_of_flue_gas(),
                    "l1": boiler_in_direct_inst.perct_of_heat_loss_in_dry_flue_gas_L1(),
                    "l2": boiler_in_direct_inst.heat_loss_due_to_formation_of_water_from_H2_in_fuel_L2(),
                    "l3": boiler_in_direct_inst.heat_loss_due_to_moisture_in_fuel_L3(),
                    "l4": boiler_in_direct_inst.heat_loss_due_to_moisture_in_air_L4(),
                    "l5": boiler_in_direct_inst.heat_loss_due_to_partial_conversion_of_C_to_CO_L5(),
                    "l6": boiler_in_direct_inst.heat_loss_due_to_radiation_and_convection_L6(),
                    "l7": boiler_in_direct_inst.heat_loss_due_to_unburnt_in_fly_ash_L7(),
                    "L7btm": boiler_in_direct_inst.heat_loss_due_to_un_burnt_in_bottom_ash_L7()
                }), status=status.HTTP_200_OK)
            else:
                return Response({"error": "error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)