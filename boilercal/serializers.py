from abc import ABC

from django.db import models
from rest_framework import serializers


class BoilerIndirectViewModel(models.Model):
    percOfO2InFlueGas = models.FloatField(blank =False)
    percOfCo2InFlueGas = models.FloatField(blank =False)
    percOfCoInflueGas = models.FloatField(blank =False)
    avgFlueGasTempInDegrees = models.FloatField(blank =False)
    ambientTempInDegress = models.FloatField(blank =False)
    humidityInAmbientAirInKgPerKgOfDa = models.FloatField(blank =False)
    ratioOfBottomAshToFlyAsh = models.FloatField(blank =False)
    carbonContentInFuelInPerc = models.FloatField(blank =False)
    hydrogenContentInFuelInPerc = models.FloatField(blank =False)
    nitrogenContentInFuelInPerc = models.FloatField(blank =False)
    oxygenContentInFuelInPerc = models.FloatField(blank =False)
    ashContentInFuelInPerc = models.FloatField(blank =False)
    moistureInFuelInPerc = models.FloatField(blank =False)
    sulphurInFuelInPerc = models.FloatField(blank =False)
    gcvOfFuelKiloCalPerKg = models.FloatField(blank =False)
    percOfBottomAsh = models.FloatField(blank =False)
    percOfFlyAsh = models.FloatField(blank =False)
    gcvOfFlyAshInKiloCalPerKg = models.FloatField(blank =False)
    gcvOfBottomAshInKiloCalPerKg = models.FloatField(blank =False)

    class Meta:
        managed = False


class EnthalpyGenericViewModel(models.Model):
    pressure = models.FloatField(blank=True)
    temperature = models.FloatField(blank=True)

    class Meta:
        managed = False


class BoilerIndirectVMSerializer(serializers.ModelSerializer):
    class Meta:
        model = BoilerIndirectViewModel
        fields = '__all__'


class EnthalpySuperHeatedSteamSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnthalpyGenericViewModel
        fields = '__all__'


class EnthalpyForSaturatedSteamSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnthalpyGenericViewModel
        fields = '__all__'


class EnthalpyForSaturatedLiquidSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnthalpyGenericViewModel
        fields = '__all__'

