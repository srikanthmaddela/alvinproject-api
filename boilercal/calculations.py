from boilercal import constants


class BoilerEfficiency:
    def __init__(self, steam_flow_per_hour, main_stream_enthalpy, feed_water_enthalpy, mass_of_fuel_per_hr,
                 gcv_of_fuel):
        self.steam_flow_per_hour = steam_flow_per_hour
        self.main_stream_enthalpy = main_stream_enthalpy
        self.feed_water_enthalpy = feed_water_enthalpy
        self.mass_of_fuel_per_hr = mass_of_fuel_per_hr
        self.gcv_of_fuel = gcv_of_fuel
        pass

    __doc__ = "(Steam flow per hour * (main steam enthalpy-feed water enthalpy) / (Mass of fuel per hour * GCV of " \
              "fuel)) * 100 "

    def boiler_efficiency_using_direct_method(self):
        efficiency = self.steam_flow_per_hour * (self.main_stream_enthalpy - self.feed_water_enthalpy) * 100 / (
                self.mass_of_fuel_per_hr * self.gcv_of_fuel)
        return efficiency


class BoilerEfficiencyIndirectMethod:
    def __init__(self, data):
        self.perc_of_o_two_in_flue_gas = data.get("percOfO2InFlueGas")
        self.perc_of_co_two_in_flue_gas = data.get("percOfCo2InFlueGas")
        self.co_in_flue_gas_CO = data.get("percOfCoInflueGas")
        self.avg_flue_gas_temp = data.get("avgFlueGasTempInDegrees")
        self.ambient_temp = data.get("ambientTempInDegress")
        self.humidity_in_ambient_air = data.get("humidityInAmbientAirInKgPerKgOfDa")
        self.ratio_of_bottom_ash_to_fly_ash = data.get("ratioOfBottomAshToFlyAsh")
        self.carbon_content_in_fuel = data.get("carbonContentInFuelInPerc")
        self.hydrogen_content_in_fuel_H2 = data.get("hydrogenContentInFuelInPerc")
        self.nitrogen_content_in_fuel = data.get("nitrogenContentInFuelInPerc")
        self.oxygen_content_in_fuel = data.get("oxygenContentInFuelInPerc")
        self.ash_content_in_fuel = data.get("ashContentInFuelInPerc")
        self.moisture_in_fuel = data.get("moistureInFuelInPerc")
        self.sulphur_in_fuel = data.get("sulphurInFuelInPerc")
        self.gcv_of_fuel = data.get("gcvOfFuelKiloCalPerKg")
        self.perc_of_bottom_ash = data.get("percOfBottomAsh")
        self.perc_of_fly_ash = data.get("percOfFlyAsh")
        self.gcv_of_fly_ash = data.get("gcvOfFlyAshInKiloCalPerKg")
        self.gcv_of_bottom_ash = data.get("gcvOfBottomAshInKiloCalPerKg")

    def calculate_tas(self):
        __doc__ = """ calculates Theoretical air required (TAS) 11.6C+ (34.8(H2- O2/8) +4.35S) / 100 """
        tas_kg_per_kg_of_fuel = (11.6 * self.carbon_content_in_fuel + (34.8 * (
                    self.hydrogen_content_in_fuel_H2 - self.oxygen_content_in_fuel / 8) + 4.35 * self.sulphur_in_fuel)) / 100
        return tas_kg_per_kg_of_fuel

    def calculate_excess_air_supplied_ea(self):
        __doc__ = """Calculate Excess air supplied self.oxygen_content_in_fuel / (21 - self.oxygen_content_in_fuel) * 100."""
        percentage_of_ea = self.perc_of_o_two_in_flue_gas * 100 / (21 - self.perc_of_o_two_in_flue_gas)
        return percentage_of_ea

    def cal_actual_mass_of_air_supplied_aas(self):
        __doc__ = """Calculate Actual air supplied (1 + self.calculate_excess_air_supplied_ea() / 100) * 
        self.calculate_tas(). """
        aas = (1 + self.calculate_excess_air_supplied_ea() / 100) * self.calculate_tas()
        return aas

    def total_mass_of_flue_gas(self):
        __doc__ = """Mass of actual air supplied/kg of fuel (ASS) + 1 kg of fuel."""
        c_one = ((self.carbon_content_in_fuel / 100) * constants.MOLECULEAR_WEIGHT_OF_CO2 / constants.MOLECULEAR_WEIGHT_OF_CARBON)
        n_one = (self.nitrogen_content_in_fuel / 100)
        o_one = ((self.cal_actual_mass_of_air_supplied_aas() - self.calculate_tas()) * constants.PERC_OF_OXYGEN_CONTENT_IN_AIR) / 100
        n_two = self.cal_actual_mass_of_air_supplied_aas() * constants.PERC_OF_NITROGEN_CONTENT_IN_AIR / 100
        s_one = ((self.sulphur_in_fuel / 100) * constants.MOLECULEAR_WEIGHT_OF_SO2 ) / constants.MOLECULEAR_WEIGHT_OF_SULPHUR
        mass_of_flue_gas = c_one + n_one + o_one + n_two + s_one
        return mass_of_flue_gas

    def perct_of_heat_loss_in_dry_flue_gas_L1(self):
        __doc__ = """Percentage of Heat loss in dry flue gas (L1)."""
        return self.total_mass_of_flue_gas() * constants.SPECIFIC_HEAT_OF_FLUE_GAS_CPF * (
                    self.avg_flue_gas_temp - self.ambient_temp) * 100 / self.gcv_of_fuel

    def heat_loss_due_to_formation_of_water_from_H2_in_fuel_L2(self):
        __doc__ = """Percentage of Heat loss due to formation of water from H2 in fuel (L2)."""
        return (9 * (self.hydrogen_content_in_fuel_H2 / 100) * (584 + constants.SPECIFIC_HEAT_OF_STEAM_CPS * (
                    self.avg_flue_gas_temp - self.ambient_temp)) * 100 / self.gcv_of_fuel)

    def heat_loss_due_to_moisture_in_fuel_L3(self):
        __doc__ = """Percentage of Heat loss due to moisture in fuel (L3)."""
        m = self.moisture_in_fuel / 100
        return m * (584 + constants.SPECIFIC_HEAT_OF_STEAM_CPS * (
                    self.avg_flue_gas_temp - self.ambient_temp)) * 100 / self.gcv_of_fuel

    def heat_loss_due_to_moisture_in_air_L4(self):
        __doc__ = """Percentage of Heat loss due to moisture in air (L4)."""
        return self.cal_actual_mass_of_air_supplied_aas() * self.humidity_in_ambient_air * constants.SPECIFIC_HEAT_OF_STEAM_CPS * (
                        self.avg_flue_gas_temp - self.ambient_temp) * 100 / self.gcv_of_fuel

    def heat_loss_due_to_partial_conversion_of_C_to_CO_L5(self):
        __doc__ = """Percentage of Heat loss due to partial conversion of C to CO (L5)."""
        return (self.co_in_flue_gas_CO * (self.carbon_content_in_fuel / 100) * 5654 * 100) /((self.co_in_flue_gas_CO + self.perc_of_co_two_in_flue_gas) * self.gcv_of_fuel)

    def heat_loss_due_to_radiation_and_convection_L6(self):
        __doc__ = """Percentage of Heat loss due to radiation and convection (L6)."""
        return 1.0

    def heat_loss_due_to_unburnt_in_fly_ash_L7(self):
        __doc__ = """Percentage of Heat loss due to unburnt in fly ash (L7)."""
        return ((self.perc_of_fly_ash / 100) * (
                    self.ash_content_in_fuel / 100) * self.gcv_of_fly_ash * 100 / self.gcv_of_fuel)

    def heat_loss_due_to_un_burnt_in_bottom_ash_L7(self):
        __doc__ = """Percentage of heat loss due to un burnt in bottom ash (L7)."""
        return ((self.perc_of_bottom_ash / 100) * (
                    self.ash_content_in_fuel / 100) * self.gcv_of_bottom_ash * 100 / self.gcv_of_fuel)

    def boiler_eff_indrect_method_nb(self):
        __doc__ = """ Boiler efficiency by indirect method """
        return (
                    100 - (self.perct_of_heat_loss_in_dry_flue_gas_L1() + self.heat_loss_due_to_formation_of_water_from_H2_in_fuel_L2() +
                    self.heat_loss_due_to_moisture_in_fuel_L3() + self.heat_loss_due_to_moisture_in_air_L4() + self.heat_loss_due_to_partial_conversion_of_C_to_CO_L5() +
                    self.heat_loss_due_to_radiation_and_convection_L6() + self.heat_loss_due_to_un_burnt_in_bottom_ash_L7() + self.heat_loss_due_to_unburnt_in_fly_ash_L7()))
