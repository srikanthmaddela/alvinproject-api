from django.conf.urls import url
from boilercal.views import GetEnthalpyForSuperHeatedSteam, GetEnthalpyForSaturatedSteam, GetEnthalpyForSaturatedLiquid, \
    CalculateBoilerEffDirectMethod, CalculateBoilerEffIndirectMethod

urlpatterns = [
    url(r'GetEnthalpyForSuperHeatedSteam/', GetEnthalpyForSuperHeatedSteam.as_view()),
    url(r'GetEnthalpyForSaturatedSteam/', GetEnthalpyForSaturatedSteam.as_view()),
    url(r'GetEnthalpyForSaturatedLiquid/', GetEnthalpyForSaturatedLiquid.as_view()),
    url(r'calculateBoilerEfficiencyDirectMethod', CalculateBoilerEffDirectMethod.as_view()),
    url(r'calculateBoilerEfficiencyInDirectMethod', CalculateBoilerEffIndirectMethod.as_view())
]