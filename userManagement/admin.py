from django.contrib import admin

# Register your models here.
from userManagement.models import EplantUser, Accounts, Address

admin.site.register(EplantUser)
admin.site.register(Accounts)
admin.site.register(Address)