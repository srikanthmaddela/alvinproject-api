import requests
from rest_framework import status
from rest_framework.response import Response

from alvinproj.settings import BASE_URL


def verify_token(request):
    try:
        auth_token = request.META.get('HTTP_AUTHORIZATION')
        token_check = requests.post(BASE_URL+'api/user/verify-token', {'token': auth_token})
        token_check_json = token_check.json()
        if token_check.status_code != 200:
            return False
        if "non_field_errors" in token_check_json:
            return False
        else:
            return True
    except:
        return False