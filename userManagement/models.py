from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, AbstractUser


# Create your models here.
from rest_framework import serializers

from alvinproj.models import BaseModel


class EplantUser(AbstractUser):
    username = models.CharField('username', max_length=150)
    email = models.EmailField('email address', unique=True)
    phone = models.CharField(max_length=20)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'password']


class EplantUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = EplantUser
        fields = ('username', 'email', 'phone', 'password')


class Accounts(BaseModel):
    CompanyName = models.CharField(max_length=150)
    Category = models.CharField(max_length=150)
    OrgEmail = models.EmailField()
    AddressId = models.ForeignKey('Address')
    ContactId = models.ForeignKey('Contact')


class AccountsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Accounts
        fields = ('CompanyName', 'Category', 'OrgEmail', 'AddressId', 'ContactId')

    def create(self, validated_data):
        acc_info = Accounts.objects.create(validated_data)
        acc_info.save()
        return acc_info


class Address(BaseModel):
    AddressLineOne = models.CharField(max_length=100)
    AddressLineTwo = models.CharField(max_length=100)
    City = models.CharField(max_length=50)
    ZipCode = models.CharField(max_length=25)
    State = models.CharField(max_length=25)


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('AddressLineOne', 'AddressLineTwo', 'City', 'ZipCode', 'State')

    def create(self, validated_data):
        address_info = Address.objects.create(**validated_data)
        address_info.save()
        return address_info


class Contact(BaseModel):
    FirstName = models.CharField(max_length=25)
    LastName = models.CharField(max_length=25)
    Phone = models.CharField(max_length=20)
    Email = models.EmailField()


class ContactSerializer(serializers.ModelSerializer):
        class Meta:
            model = Contact
            fields = ('FirstName', 'LastName', 'Phone', 'Email')

        def create(self, validated_data):
            contact_info = Contact.objects.create(**validated_data)
            contact_info.save()
            return contact_info


class UserAccounts(BaseModel):
    UserId = models.ForeignKey(EplantUser)
    AccountId = models.ForeignKey(Accounts)


class UserAccountsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccounts
        fields = ('UserId', 'AccountId')

    def create(self, validated_data):
        userAccInfo = UserAccounts.objects.create(**validated_data)
        userAccInfo.save()
        return userAccInfo