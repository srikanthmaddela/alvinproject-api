from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from rest_framework_jwt.views import verify_jwt_token, obtain_jwt_token

from userManagement.views import LoginUser, RegisterUser, Logout

urlpatterns = [
    url(r'login', LoginUser.as_view()),
    url(r'generate-token', obtain_jwt_token),
    url(r'verify-token', verify_jwt_token),
    url(r'register', RegisterUser.as_view()),
    url(r'logout', Logout.as_view())
]