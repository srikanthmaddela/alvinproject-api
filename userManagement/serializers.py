from rest_framework import serializers

from userManagement.models import EplantUser, Accounts, EplantUserSerializer, AddressSerializer, ContactSerializer, \
    AccountsSerializer


class RegisterUserSerializer(serializers.Serializer):
    UserInfo = EplantUserSerializer()
    Address = AddressSerializer()
    ContactInfo = ContactSerializer()
    AccountInfo = AccountsSerializer()


class LoginUserSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(max_length=10, required=True)