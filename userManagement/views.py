import requests
from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import ValidationError
from django.shortcuts import render

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.authentication import BaseAuthentication
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.views import verify_jwt_token

from alvinproj.settings import BASE_URL
from alvinproj.utils import CsrfExemptSessionAuthentication
from userManagement.decorators import verify_token
from userManagement.models import EplantUser, Accounts, Address, Contact, EplantUserSerializer, AddressSerializer, \
    ContactSerializer, AccountsSerializer
from userManagement.serializers import RegisterUserSerializer, LoginUserSerializer


class LoginUser(APIView):
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(request_body=LoginUserSerializer)
    def post(self, request):
        user_model = get_user_model()
        try:
            serializer = LoginUserSerializer(data=request.data)
            if serializer.is_valid():
                user = user_model.objects.get(email=request.data.get("email"))
                if user:
                    auth_user = authenticate(email=request.data["email"], password=request.data["password"], type='user')
                    if auth_user:
                        auth_token_info = requests.post(BASE_URL + "api/user/generate-token",
                                                        data={"email": request.data["email"],
                                                              "password": request.data["password"]})
                        auth_token_info = auth_token_info.json()
                        login(request, auth_user)
                        return Response({
                            "username": user.username,
                            "token": auth_token_info.get("token"),
                            "success": True
                        }, status=status.HTTP_200_OK)
                    else:
                        return Response({"error": "Invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED)
                else:
                    return Response({"error": "No user found"}, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({"error": serializer.errors}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response({"error": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RegisterUser(APIView):
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(request_body=RegisterUserSerializer)
    def post(self, request):
        try:
            e_plant_user_info = EplantUserSerializer(data=request.data["UserInfo"])
            if e_plant_user_info.is_valid() is False:
                return Response({"error": "UserInfo object is not valid"}, status=status.HTTP_206_PARTIAL_CONTENT)
            address_info = AddressSerializer(data=request.data["Address"])
            if address_info.is_valid() is False:
                return Response({"error": "Address object is not valid"}, status=status.HTTP_206_PARTIAL_CONTENT)
            contact_info = ContactSerializer(data=request.data["ContactInfo"])
            if contact_info.is_valid() is False:
                return Response({"error": "contactInfo object is not valid"}, status=status.HTTP_206_PARTIAL_CONTENT)
            address_info = address_info.create(address_info.validated_data)
            contact_info = contact_info.create(contact_info.validated_data)
            acc_info = request.data["AccountInfo"]
            acc_info["AddressId"] = address_info
            acc_info["ContactId"] = contact_info
            acc_ser_info = AccountsSerializer(data=acc_info)
            if acc_ser_info.is_valid() is False:
                return Response({"error": "AccountInfo object is not valid"}, status=status.HTTP_206_PARTIAL_CONTENT)
            else:
                try:
                    EplantUser.objects.get(email=e_plant_user_info.validated_data.get("email"))
                    return Response({"error": "User already exsists"}, status=status.HTTP_207_MULTI_STATUS)
                except Exception as e:
                    new_user = EplantUser.objects.create_user(username=e_plant_user_info.validated_data.get("username"),
                                                              email=e_plant_user_info.validated_data.get("email"),
                                                              password=request.data["UserInfo"].get("password"),
                                                              phone=e_plant_user_info.validated_data.get("phone"))
                    new_user.save()
                    auth_user = authenticate(email=new_user.email, password=request.data["UserInfo"].get("password"),
                                             type='user')
                    if auth_user:
                        user_info = {"email": e_plant_user_info.validated_data.get("email"),
                                     "password": request.data["UserInfo"].get("password")}
                        token_resp = requests.post(BASE_URL + "api/user/generate-token", data=user_info)
                        token_resp = token_resp.json()
                        login(request, auth_user)
                        user_model = get_user_model()
                        user = user_model.objects.get(email=e_plant_user_info.validated_data.get("email"))
                        return Response({"username": user.username, "authToken": token_resp.get("token")},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"error": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            return Response({"error": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class Logout(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request):
        try:
            if request.user:
                return Response(True, status=status.HTTP_200_OK)
            else:
                return Response(False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except:
            return Response(False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
