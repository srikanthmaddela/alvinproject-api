from django.db import models
from django.utils import six, timezone


class BaseModel(models.Model):
        CreatedOn = models.DateTimeField('created on', default=timezone.now)
        ModifiedOn = models.DateTimeField('modified on', default=timezone.now)
        IsDeleted = models.BooleanField('Is deleted', default=False)