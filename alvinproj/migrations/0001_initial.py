# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-07-14 12:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaseModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CreatedOn', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'created on')),
                ('ModifiedOn', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'modified on')),
                ('IsDeleted', models.BooleanField(default=False, verbose_name=b'Is deleted')),
            ],
        ),
    ]
