# Download python latest version

Link:


      https://www.python.org/downloads/

# Install python in your machine

Link:

      https://www.youtube.com/watch?v=dX2-V2BocqQ

# Install PIP
Follow the Instructions provided here https://pip.pypa.io/en/stable/installing/ (or) Get the pip from https://bootstrap.pypa.io/get-pip.py and run " python get-pip.py " 
Note: for the windows users set pip exceutable path in your system environment variable path called PATH.

# Install virtualenv

Link:

      https://virtualenv.pypa.io/en/stable/installation/

For windows: open command prompt -> pip install virtualenv

# create a virtualenv

Create a folder anywhere in your PC with any of your favourite name.
Go to the folder path from command prompt.
Run follwing command to create a virtual env.

# virtualenv <your virtualenv name>

Example:

      1. C:\srikanth\softwares\venvs

      2. virtualenv myenv

# Activte virtaulenv

command: go to the virtual environment folder from command prompt and go to scripts folder in it. type "activate"

Example:

      C:\srikanth\softwares\venvs\myenv\Scripts\activate

It should show the following

Example:

      (myenv) C:\srikanth\softwares\venvs\myenv\Scripts>

# Deactivate virtaulenv

command: go to the virtual environment folder from command prompt and go to scripts folder in it. type "deactivate"

Example:

      C:\srikanth\softwares\venvs\myenv\deactivate

# CLONING, INSTALLING DEPENDENCIES AND RUNNING THE PROJECT

1. Clone the project into your PC

Example:

       1. C:\srikanth\softwares\eplant-api
       2. cmd git clone https://srikanthmaddela@bitbucket.org/srikanthmaddela/alvinproject-api.git

2. Get the path of the requirements.txt file in the cloned project. copy the path.

3. Go to the virtual environment folder which you have created earlier from command prompt.

Example:

      C:\srikanth\softwares\venvs\myenv
   
4. Activate the virtaul environment

5. Type pip install -r "path/to/requirements.txt". It Will install all your project dependencies

Example:

         1. C:\srikanth\softwares\venvs\myenv
         2. C:\srikanth\softwares\venvs\myenv\Scripts\activate
	     3. pip install -r "C:\srikanth\softwares\eplant-api\alvinproject\requirements.txt"
	   
6. After all the packages were installed. go to the project folder from the virtualenv path.

Example:

      1. cd C:\srikanth\softwares\eplant\alvinproject-api

7. Now the path should look like below

Example:

      1. (myenv) C:\srikanth\softwares\eplant\alvinproject-api

8. Run "python manage.py runserver"(without quotemarks) in your cmd prompt. this will run your project.

After sucessfull run you will see the following output in your command prompt.

Output:

      System check identified no issues (0 silenced).

      October 01, 2018 - 22:38:51

      Django version 1.10, using settings 'alvinproj.settings'

      Starting development server at http://127.0.0.1:8000/

      Quit the server with CTRL-BREAK.

Thats it. copy the url and put it on your favouite broswer and you will be seeing the project swagger page.

# Run migrations to create tables in the database

1. Go to the project folder and run the two commands 
      1. "python manage.py makemigrations"(without quotemarks)

      2. "python manage.py migrate"(without quotemarks)

      Example:

          1. (myenv) C:\srikanth\softwares\eplant\alvinproject-api
      
          2. python manage.py makemigrations
          
          3. python manage.py migrate
          
2. Now again run the project with "python manage.py runserver"(without quotemarks)"

